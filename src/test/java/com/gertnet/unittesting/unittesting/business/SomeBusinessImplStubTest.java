package com.gertnet.unittesting.unittesting.business;

import com.gertnet.unittesting.unittesting.data.SomeDataService;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * User: gert Date: 2019/08/19 Time: 20:41
 */
public class SomeBusinessImplStubTest {

    class SomeDataServiceStub implements SomeDataService {

        int[] data;

        public SomeDataServiceStub(int[] data) {
            this.data = data;
        }

        @Override
        public int[] retrieveAllData() {
            return data;
        }
    }

    @Test
    public void calculateSumUsingDataService_basic() {

        SomeBusinessImpl business = new SomeBusinessImpl();
        business.setSomeDataService(new SomeDataServiceStub(new int[] { 10, 20, 30 }));

        int actualResult = business.calculateSumUsingDataService();
        int expectedResult = 60;

        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void calculateSumUsingDataService_empty() {

        SomeBusinessImpl business = new SomeBusinessImpl();
        business.setSomeDataService(new SomeDataServiceStub(new int[] {}));

        int actualResult = business.calculateSumUsingDataService();
        int expectedResult = 0;

        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void calculateSumUsingDataService_single() {

        SomeBusinessImpl business = new SomeBusinessImpl();
        business.setSomeDataService(new SomeDataServiceStub(new int[] { 10 }));

        int actualResult = business.calculateSumUsingDataService();
        int expectedResult = 10;

        assertEquals(expectedResult, actualResult);

    }
}
