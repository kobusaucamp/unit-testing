package com.gertnet.unittesting.unittesting.business;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * User: gert Date: 2019/08/19 Time: 20:41
 */
public class SomeBusinessImplTest {

    @Test
    public void calculateSum_basic() {

        SomeBusinessImpl business = new SomeBusinessImpl();

        int actualResult = business.calculateSum(new int[] { 10, 20, 30 });
        int expectedResult = 60;

        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void calculateSum_empty() {

        SomeBusinessImpl business = new SomeBusinessImpl();

        int actualResult = business.calculateSum(new int[] {});
        int expectedResult = 0;

        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void calculateSum_single() {

        SomeBusinessImpl business = new SomeBusinessImpl();

        int actualResult = business.calculateSum(new int[] { 10 });
        int expectedResult = 10;

        assertEquals(expectedResult, actualResult);

    }
}
