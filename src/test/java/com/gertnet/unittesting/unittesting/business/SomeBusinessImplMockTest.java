package com.gertnet.unittesting.unittesting.business;

import com.gertnet.unittesting.unittesting.data.SomeDataService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * User: gert Date: 2019/08/19 Time: 20:41
 */
@RunWith(MockitoJUnitRunner.class)
public class SomeBusinessImplMockTest {

    @InjectMocks
    SomeBusinessImpl business;

    @Mock
    SomeDataService dataServiceMock = mock(SomeDataService.class);

    @Test
    public void calculateSumUsingDataService_basic() {
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[] { 10, 20, 30 });
        assertEquals(60, business.calculateSumUsingDataService());
    }

    @Test
    public void calculateSumUsingDataService_empty() {
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[] {});
        assertEquals(0, business.calculateSumUsingDataService());
    }

    @Test
    public void calculateSumUsingDataService_single() {
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[] { 10 });
        assertEquals(10, business.calculateSumUsingDataService());
    }
}
